---
layout: job_family_page
title: "Incubation Engineers"
description: "Incubation Engineers are experienced Full Stack Software Engineers with deep knowledge in a specific domain that are able to work independently to deliver a solution to market."
---

Incubation Engineers are experienced Software Engineers with deep knowledge in a specific domain that are able to work independently to deliver a solution to market.  The goal of an Incubation Engineer is to reach a [Viable](https://about.gitlab.com/direction/maturity/#legend) level of maturity of their respective area within the GitLab project. 

Incubation Engineers work within the [Incubation Engineering Department](https://about.gitlab.com/handbook/engineering/incubation/).

## Job Grade

The Incubation Engineer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

The Senior Incubation Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

The Staff Incubation Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


## Requirements

### Incubation Engineer

* Good technical skills - Ability to work across backend and frontend.  Experience with Ruby, Go, or Vue.js are particularly relevant given GitLab’s current tech stack.
* Desire to work in a “startup within a startup” with a broad scope of ownership and high degree of autonomy.
* Demonstrated interest and experience in the subject matter.
* They must be excited about the ability to work independently, or have prior success in a similar model.
* Understand the competitive landscape and market opportunities in their area.
* Be comfortable with changing direction based on internal and external stakeholder feedback and market conditions.
* Experience across product management and software engineering. You'll have worked as an early-stage team member at start-ups or on projects that are pre-market fit. 
* Ability to discuss tactics and strategies that will enable you to develop a vision for the SEG and deliver upon it iteratively. 
* Ability to deliver despite ambiguity.
* Ability to build and sustain relationships with internal and external stakeholders.
* Understand and articulate market fit for the Incubation project you are leading.

### Senior Incubation Engineer

* The engineer must meet the requirements for an [Intermediate Incubation Engineer](https://about.gitlab.com/job-families/engineering/incubation-engineer/#incubation-engineer) (or above).
* Great technical skills - Ability to work across backend, frontend, and infrastructure management code
* Ability to instrument qualitative and quantitative sensing mechanisms that will guide decisions and directions. 
* Extensive experience across product management and software engineering. You'll have worked as a founding or early-stage team member at start-ups or on projects that are pre-market fit. 

### Staff Incubation Engineer

* The engineer must meet the requirements for a [Senior Incubation Engineer](https://about.gitlab.com/job-families/engineering/incubation-engineer/#senior-incubation-engineer) (or above).
* Exemplary technical skills and architectural ability across front end, backend and Infrastructure. 
* Ability to discuss tactics and strategies that will enable you to contribute to the vision for the SEG and deliver upon it iteratively.
* Ability to contribute to qualitative and quantitative sensing mechanisms that will help inform decisions and directions.
* Able to take on team leadership and mentoring responsibilities to provide guidance and support across the team.

## Nice-to-haves

* Being a prior company technical cofounder
* Being an early contributor to a successful open source project
* Working successfully on a prior Incubation Engineering project
* Extensive familiarity with the GitLab project and application
* Experience working with people around the globe

## Incubation Engineer Responsibilities

* Develop features and improvements in a secure, well-tested, and performant way
* Work closely with company leadership to guide GitLab’s entry into the market represented by the SEG
* Interact with community members and help drive initial user adoption
* Develop strategies for scaling beyond the Incubation phase
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Regularly [demonstrate](/handbook/engineering/#engineering-demo-process) progress to stake holders
* Contribute to documentation and UI design as required  

## Incubation Engineering Performance Indicators

Incubation Engineers have the following Performance Indicators

* [SEG Category Maturity](/handbook/engineering/incubation/performance-indicators/#seg-category-maturity)
* [Community Engagement](/handbook/engineering/incubation/performance-indicators/#community-engagement)

## Director, Incubation Engineering

The  Director, Incubation Engineering is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Director, Incubation Engineering Responsibilities

* Manage and help set the direction for 4-6 Incubation Engineers across a range of domains 
* Recruit top talent to build a best-in-class Incubation Engineering Department; responsible for hiring and capacity planning
* Develop strategies to support scaling beyond the Incubation phase and advise on the project lifecycle for their team
* Collaborate with Engineering peers and company executives on key innovation initiatives
* Provide transparent status to stakeholders
* Support SEGs by validating and debating approaches to test ideas
* Serve as champion of the Incubation department and the work that the engineers engage in 
* Stay abreast of long term technical trends in the market
* Stay informed about other products' functionality and positioning
* Advocate for broad changes in the GitLab project to stay ahead of scalability and performance bottlenecks

### Director, Incubation Engineering Requirements

* Demonstrated success in leading teams of engineers or other technical experts in a fast moving, high growth environment
* Experience scaling products without compromising on security 
* Broad knowledge of technology trends with a focus on managing innovation / incubation / accelerator teams
* Diplomatic communication skills with the ability to utilise storytelling and narration to evangelize the Incubation Engineering Department’s story 
Experience in a variety of stages of the startup lifecycle, from inception to launch

### Director, Incubation Engineering Performance Indicators

* Hiring Actual vs Pln
* Team MR Rate
* Community Contribution rate
* Team Marketing activities

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next candidates will meet the Hiring Manager.
* There will then be 2x Technical Interviews - one Frontend and one Backend.
* The next _optional_ step is an architectural discussion
* Next you will meet a Product Manager
* The Final interview is with a senior member (Staff Engineer or Director) of the Incubation Engineering Department

Additional details about our process can be found on our [hiring page](/handbook/hiring)
